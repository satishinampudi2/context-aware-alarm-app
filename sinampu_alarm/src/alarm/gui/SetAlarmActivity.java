package alarm.gui;

import jade.core.MicroRuntime;
import jade.util.Logger;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

import alarm.agent.AlarmAgentInterface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;

public class SetAlarmActivity extends Activity {

	private static final int SLEEPTIME_DIALOG_ID = 123;
	private static final int ALARMTIME_DIALOG_ID = 777;
	private static final int PENDINGINTENT_ID = 0;

	private static final String CLASS = "class";
	private static final String EXAM = "exam";
	private static final String BUS = "bus";
	private static final String FLIGHT = "flight";
	protected static int snoozeCounter = 0;
	// Flight delay in hours
	// Bus delay in minutes
	private static final int FIVE_HOURS = 5 * 60 * 60 * 1000;

	private Logger logger = Logger.getJADELogger(this.getClass().getName());
	private HashMap<String, GCalEvent> calEvents = new HashMap<String, GCalEvent>();
	private ArrayList<String> friends = new ArrayList<String>();

	private Button addButton;
	private Button delButton;
	private Button sleepButton;
	private ListView listView;
	private TextView notificationTView;
	private MyReceiver myReceiver;
	private int hourOfDay;
	private int minuteOfDay;
	private String sleepTime;
	private Location location;
	private ArrayAdapter<String> arAdapter;
	private String nickname;
	private AlarmAgentInterface alarmAgentInterface;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			nickname = extras.getString("nickname");
		}

		try {
			alarmAgentInterface = MicroRuntime.getAgent(nickname).getO2AInterface(AlarmAgentInterface.class);
		} catch (StaleProxyException e) {
			showAlertDialog(getString(R.string.msg_interface_exc), true);
		} catch (ControllerException e) {
			showAlertDialog(getString(R.string.msg_controller_exc), true);
		}

		setContentView(R.layout.main_activity);
		notificationTView = (TextView) findViewById(R.id.notification);

		listView = (ListView) findViewById(R.id.list);
		arAdapter = new ArrayAdapter<String>(SetAlarmActivity.this, R.layout.alarmrow, new ArrayList<String>());
		listView.setAdapter(arAdapter);
		listView.setOnItemClickListener(listClickListener);

		initButtons();
		getLocation();
		myReceiver = new MyReceiver();

		IntentFilter busDelayNotifier = new IntentFilter();
		busDelayNotifier.addAction("alarm.agent.BUS_DELAY");
		registerReceiver(myReceiver, busDelayNotifier);

		IntentFilter flightDelayNotifier = new IntentFilter();
		flightDelayNotifier.addAction("alarm.agent.FLIGHT_DELAY");
		registerReceiver(myReceiver, flightDelayNotifier);

		IntentFilter msgFromFriend = new IntentFilter();
		msgFromFriend.addAction("alarm.agent.WAKE_D_FRIEND");
		registerReceiver(myReceiver, msgFromFriend);

		IntentFilter tellFriend2WakeUp = new IntentFilter();
		tellFriend2WakeUp.addAction("alarm.agent.ASKFRIEND2WAKEUP");
		registerReceiver(myReceiver, tellFriend2WakeUp);

		IntentFilter getCurrentLocation = new IntentFilter();
		getCurrentLocation.addAction("alarm.agent.GET_CURRENT_LOCATION");
		registerReceiver(myReceiver, getCurrentLocation);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		Intent activate = new Intent(SetAlarmActivity.this, AlarmReceiver.class);

		PendingIntent alarmIntent = PendingIntent.getBroadcast(SetAlarmActivity.this, PENDINGINTENT_ID, activate, 0);
		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		alarmManager.cancel(alarmIntent);

		if(arAdapter.getCount() > 1)
			arAdapter.remove(arAdapter.getItem(0));
		arAdapter.notifyDataSetChanged();
		delButton.setEnabled(false);
		
		unregisterReceiver(myReceiver);
		logger.log(Level.INFO, "Destroy activity!");
	}

	private class MyReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			logger.log(Level.INFO, "Received intent " + action);
			if (action.equalsIgnoreCase("alarm.agent.BUS_DELAY")) {
				calEvents.clear();
				ReadCalendarEvents();
				Bundle extras = intent.getExtras();
				String message = null;
				if (extras != null) {
					message = extras.getString("delay");
				}
				Integer delay = Integer.valueOf(message);
				findAndPrepone(delay);
			} else if (action.equalsIgnoreCase("alarm.agent.FLIGHT_DELAY")) {
				calEvents.clear();
				ReadCalendarEvents();
				Bundle extras = intent.getExtras();
				String message = null;
				if (extras != null) {
					message = extras.getString("delay");
				}
				Integer delay = Integer.valueOf(message);
				findAndPostpone(delay);

			} else if (action.equalsIgnoreCase("alarm.agent.WAKE_D_FRIEND")) {
				Bundle extras = intent.getExtras();
				String message = null;
				if (extras != null) {
					message = extras.getString("wakedfriend");
				}
				String[] messageContent = message.split(":");
				String friendName = messageContent[0];
				String latitude = messageContent[1];
				String longitude = messageContent[2];
				notificationTView = (TextView) findViewById(R.id.notification);
				notificationTView.setText("Wake " + friendName + "who is at Latitude: " + latitude + " and Longitude" + longitude);
			} else if (action.equalsIgnoreCase("alarm.agent.ASKFRIEND2WAKEUP")) {
				long currentTime = new Date().getTime();
				calEvents.clear();
				ReadCalendarEvents();
				if(calEvents.get(EXAM) == null){
					notificationTView.setText("Update Calendar with exam entry Please.. ");
					return;
				}
					
				long examTime = calEvents.get(EXAM).getStartDate().getTime();
				if ((examTime - currentTime) / (60 * 60 * 1000) <= 1) {
					getFriendsFromContacts();
					alarmAgentInterface.populateFriendsList(friends);
					alarmAgentInterface.requestFriends(nickname);
				}
			} else if (action.equalsIgnoreCase("alarm.agent.GET_CURRENT_LOCATION")) {
				getLocation();
				alarmAgentInterface.populateLocation(location);
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void findAndPostpone(Integer delay) {
		if (calEvents == null || calEvents.size() == 0) {
			notificationTView.setText("Update Calendar Please.. ");
			return;
		}
		if(calEvents.get(FLIGHT) == null){
			notificationTView.setText("Update Calendar with flight entry Please.. ");
			return;
		}
		Date presentationStartDate = calEvents.get(FLIGHT).getStartDate();
		String alarmTime = arAdapter.getItem(0);
		String[] alarm = alarmTime.split(":");
		Date alarmDate = new Date();
		alarmDate.setHours(Integer.valueOf(alarm[0]));
		alarmDate.setMinutes(Integer.valueOf(alarm[1]));

		String[] sleep = sleepTime.split(":");
		Date sleepDate = new Date();
		sleepDate.setHours(Integer.valueOf(sleep[0]));
		sleepDate.setMinutes(Integer.valueOf(sleep[1]));

		if (alarmDate.after(sleepDate) && (presentationStartDate.getTime() - alarmDate.getTime() <= FIVE_HOURS)) {
			double diffInHours = (double) ((alarmDate.getTime() - sleepDate.getTime()) / (1000 * 60 * 60));
			if (Math.ceil(diffInHours) < 4) {
				Date newAlarm = new Date(alarmDate.getTime() + (delay * 60 * 60 * 1000));

				Intent activate = new Intent(SetAlarmActivity.this, AlarmReceiver.class);
				PendingIntent alarmIntent = PendingIntent.getBroadcast(SetAlarmActivity.this, PENDINGINTENT_ID, activate, 0);
				AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
				alarmManager.cancel(alarmIntent);

				arAdapter.clear();

				arAdapter.add(pad(newAlarm.getHours()) + ":" + pad(newAlarm.getMinutes()));
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.HOUR_OF_DAY, newAlarm.getHours());
				calendar.set(Calendar.MINUTE, newAlarm.getMinutes());
				alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

				arAdapter.notifyDataSetChanged();
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void findAndPrepone(Integer delay) {
		if (calEvents != null || calEvents.size() != 0) {
			if(calEvents.get(CLASS) == null){
				notificationTView.setText("Update Calendar with class entry Please.. ");
				return;
			}
			Date classStartDate = calEvents.get(CLASS).getStartDate();
			String alarmTime = arAdapter.getItem(0);
			String[] alarm = alarmTime.split(":");
			Date alarmDate = new Date();
			alarmDate.setHours(Integer.valueOf(alarm[0]));
			alarmDate.setMinutes(Integer.valueOf(alarm[1]));
			ArrayList<Date> busTimings = new ArrayList<Date>();
			for (Entry<String, GCalEvent> entry : calEvents.entrySet()) {
				if (entry.getKey().contains(BUS)) {
					Date busDate = entry.getValue().getStartDate();
					long t = busDate.getTime();
					busTimings.add(new Date(t + delay * 60000));
				}
			}
			if(busTimings.size() == 0){
				notificationTView.setText("Update Calendar with bus entries Please.. ");
				return;
			}
				
			Collections.sort(busTimings);
			int i = 0;
			while (i < busTimings.size() && classStartDate.before(busTimings.get(i))) {
				i++;
			}

			Date bus2Catch = busTimings.get(i - 1);
			if(bus2Catch.after(classStartDate)){
				notificationTView.setText("Buses are running too late, first bus itself is after class");
			}
			if (alarmDate.after(bus2Catch)) {
				alterAlarm(bus2Catch);
			} else {
				double diffInMinutes = (double) ((bus2Catch.getTime() - alarmDate.getTime()) / (1000 * 60));
				if (Math.ceil(diffInMinutes) < 5 * 60 * 1000) {
					alterAlarm(bus2Catch);
				}
			}
		} else {
			notificationTView.setText("Update Calendar Please.. ");
		}
	}

	@SuppressWarnings("deprecation")
	private void alterAlarm(Date bus2Catch) {
		Date newAlarm = new Date(bus2Catch.getTime() - (5 * 60 * 1000));

		Intent activate = new Intent(SetAlarmActivity.this, AlarmReceiver.class);
		PendingIntent alarmIntent = PendingIntent.getBroadcast(SetAlarmActivity.this, PENDINGINTENT_ID, activate, 0);
		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		alarmManager.cancel(alarmIntent);

		arAdapter.clear();

		arAdapter.add(pad(newAlarm.getHours()) + ":" + pad(newAlarm.getMinutes()));
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, newAlarm.getHours());
		calendar.set(Calendar.MINUTE, newAlarm.getMinutes());
		alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

		arAdapter.notifyDataSetChanged();
	}

	private void initButtons() {
		addButton = (Button) findViewById(R.id.addAlarm);
		delButton = (Button) findViewById(R.id.delAlarm);
		sleepButton = (Button) findViewById(R.id.sleepTime);
		Button clearButton = (Button) findViewById(R.id.clearNotification);
		delButton.setEnabled(false);

		clearButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				notificationTView.setText("");
			}
		});

		addButton.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				showDialog(ALARMTIME_DIALOG_ID);
			}
		});

		sleepButton.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				showDialog(SLEEPTIME_DIALOG_ID);
			}
		});
		delButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent activate = new Intent(SetAlarmActivity.this, AlarmReceiver.class);

				PendingIntent alarmIntent = PendingIntent.getBroadcast(SetAlarmActivity.this, PENDINGINTENT_ID, activate, 0);
				AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
				alarmManager.cancel(alarmIntent);

				arAdapter.remove(arAdapter.getItem(0));
				arAdapter.notifyDataSetChanged();
				delButton.setEnabled(false);
			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case ALARMTIME_DIALOG_ID:
			TimePickerDialog alarmtimePickerDialog = new TimePickerDialog(this, timePickerListener, hourOfDay, minuteOfDay, true);
			alarmtimePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {

					if (arAdapter.getCount() == 1) {
						showAlertDialog("Cannot add more than one alarm", false);
					} else {
						Intent activate = new Intent(SetAlarmActivity.this, AlarmReceiver.class);
						PendingIntent alarmIntent = PendingIntent.getBroadcast(SetAlarmActivity.this, PENDINGINTENT_ID, activate, 0);
						AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
						String alarmTime = pad(hourOfDay) + ":" + pad(minuteOfDay);
						arAdapter.add(alarmTime);
						arAdapter.notifyDataSetChanged();

						Calendar calendar = Calendar.getInstance();
						calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
						calendar.set(Calendar.MINUTE, minuteOfDay);
						if (System.currentTimeMillis() - calendar.getTimeInMillis() < 0) // Alarm
																							// should
																							// not
																							// be
																							// set
																							// to
																							// time
																							// previous
																							// to
																							// now.
							alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
					}
				}
			});
			return alarmtimePickerDialog;
		case SLEEPTIME_DIALOG_ID:
			TimePickerDialog sleeptimePickerDialog = new TimePickerDialog(this, timePickerListener, hourOfDay, minuteOfDay, true);
			sleeptimePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					sleepTime = pad(hourOfDay) + ":" + pad(minuteOfDay);
					TextView slV = (TextView) findViewById(R.id.sleeptview);
					slV.setText(sleepTime);
				}
			});
			return sleeptimePickerDialog;
		}
		return null;
	}

	private OnItemClickListener listClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			@SuppressWarnings("unused")
			String selectTime = (String) ((ListView) parent).getItemAtPosition(position);
			delButton.setEnabled(true);
		}
	};

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hour, int minute) {
			hourOfDay = hour;
			minuteOfDay = minute;
		}
	};

	private void getLocation() {
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(true);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		String provider = locationManager.getBestProvider(criteria, true);
		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location gpsLocation) {
				location = gpsLocation;
				alarmAgentInterface.populateLocation(location);
			}

			public void onStatusChanged(String provider, int status, Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		Location gpsLocation = locationManager.getLastKnownLocation(provider);
		if (gpsLocation != null)
			location = gpsLocation;

	}

	private void showAlertDialog(String message, final boolean fatal) {
		AlertDialog.Builder builder = new AlertDialog.Builder(SetAlarmActivity.this);
		builder.setMessage(message).setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				if (fatal)
					finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void getFriendsFromContacts() {
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				friends.add(name.toLowerCase());
			}
		}
	}

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	class GCalEvent {
		private Date StartDate;
		private Date EndDate;

		public GCalEvent(Date sDate, Date eDate) {
			this.StartDate = sDate;
			this.EndDate = eDate;
		}

		public Date getStartDate() {
			return StartDate;
		}

		public Date getEndDate() {
			return EndDate;
		}
	}

	private void ReadCalendarEvents() {
		ContentResolver cr = getContentResolver();
		Cursor cursor = cr.query(Uri.parse("content://com.android.calendar/events"), new String[] { "calendar_id", "title", "description", "dtstart", "dtend", "eventLocation" }, null, null, null);
		cursor.moveToFirst();
		String[] CalNames = new String[cursor.getCount()];
		for (int i = 0; i < CalNames.length; i++) {
			String title = cursor.getString(1);
			if (title != null && (title.toLowerCase().startsWith(CLASS) || title.toLowerCase().startsWith(FLIGHT) || title.toLowerCase().startsWith(EXAM) || title.toLowerCase().startsWith(BUS)))
				calEvents.put(title.toLowerCase(), new GCalEvent(new Date(cursor.getLong(3)), new Date(cursor.getLong(4))));
			cursor.moveToNext();
		}
		cursor.close();
	}
}
