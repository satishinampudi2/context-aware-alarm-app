package alarm.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.*;

public class AlarmAgent extends Agent implements AlarmAgentInterface {

	private static final long serialVersionUID = 1594381294421614291L;

	private Context context;

	private static final String LOCATION_REQUEST = "__asklocation__";
	private static final String LOCATION_INFO = "__telllocation__";
	private static final String WAKEOTHERGUYUPMESSAGE = "__wakeotherguyupmessage__";
	private ArrayList<String> friendsList = new ArrayList<String>();
	private AMSAgentDescription[] agents = null;
	private HashMap<String, ACLMessage> locationMsgReceivedFrom = new HashMap<String, ACLMessage>();
	private Logger logger = Logger.getLogger("");
	private Location currentLocation;

	protected void setup() {
		Object[] args = getArguments();
		if (args != null && args.length > 0) {
			if (args[0] instanceof Context) {
				context = (Context) args[0];
			}
		}

		/*
		 * Following three behaviors are Cyclic behaviors. Listens to message
		 * from BusAgent Listens to message from FlightAgent Listens to message
		 * from friends - who request for location, upon responding with
		 * location if I am nearest, then he will ask me to wake him up.
		 */
		addBehaviour(new BusDelayListener(this));
		addBehaviour(new FlightDelayListener(this));
		addBehaviour(new ListenToFriends(this));

		registerO2AInterface(AlarmAgentInterface.class, this);

		/*
		 * setup() is called after user's nickname is entered and Alarm! button
		 * is pressed in opening screen(MainActivity). This will send broadcast
		 * to start activity SetAlarmActivity.
		 */
		Intent broadcast = new Intent();
		broadcast.setAction("alarm.agent.SHOW_ALARM");
		logger.log(Level.INFO, "Sending broadcast " + broadcast.getAction());
		context.sendBroadcast(broadcast);

		/*
		 * Gets the current location when this agent is created. Whenever user
		 * changes his location, that currentlocation information is updated
		 * using populateLocation method. This method is invoked in
		 * LocationListener of SetAlarmActivity whenever location is updated.
		 */
		getCurrentLocation();

	}

	protected void takeDown() {
	}

	/*
	 * Following four methods are for sending message to SetAlarmActivity Their
	 * listener is MyReciever class in SetAlarmActivity.
	 */

	private void notifyBusDelay2App(String s) {
		Intent broadcast = new Intent();
		broadcast.setAction("alarm.agent.BUS_DELAY");
		broadcast.putExtra("delay", s);
		context.sendBroadcast(broadcast);
	}

	private void notifyAirDelay2App(String s) {
		Intent broadcast = new Intent();
		broadcast.setAction("alarm.agent.FLIGHT_DELAY");
		broadcast.putExtra("delay", s);
		context.sendBroadcast(broadcast);
	}

	private void notify2WakeDFriend2App(String s) {
		Intent broadcast = new Intent();
		broadcast.setAction("alarm.agent.WAKE_D_FRIEND");
		broadcast.putExtra("wakedfriend", s);
		/*
		 * It reads wake the friend. s is friend's name. s is from friend who
		 * asked us to wake him up
		 */
		context.sendBroadcast(broadcast);
	}

	private void getCurrentLocation() {
		Intent broadcast = new Intent();
		broadcast.setAction("alarm.agent.GET_CURRENT_LOCATION");
		context.sendBroadcast(broadcast);
	}

	class BusDelayListener extends CyclicBehaviour {

		private static final long serialVersionUID = 1L;
		private MessageTemplate template = MessageTemplate.MatchConversationId("BUS_DELAYED");

		public BusDelayListener(AlarmAgent alarmAgent) {
			super(alarmAgent);
		}

		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(template);
			if (msg != null) {
				String[] messageReceived = msg.getContent().split(":");
				if (myAgent.getLocalName().equalsIgnoreCase(messageReceived[0])) {
					notifyBusDelay2App(messageReceived[1]);
				}
			}
		}
	}

	class FlightDelayListener extends CyclicBehaviour {

		private static final long serialVersionUID = 1L;
		private MessageTemplate template = MessageTemplate.MatchConversationId("FLIGHT_DELAYED");

		public FlightDelayListener(AlarmAgent alarmAgent) {
			super(alarmAgent);
		}

		@Override
		public void action() {
			ACLMessage msg = myAgent.receive(template);
			if (msg != null) {
				String[] messageReceived = msg.getContent().split(":");
				if (messageReceived[0].equals(myAgent.getLocalName())) {
					notifyAirDelay2App(messageReceived[1]);
				}
			}
		}
	}

	class ListenToFriends extends CyclicBehaviour {

		private static final long serialVersionUID = 1L;

		public ListenToFriends(AlarmAgent alarmAgent) {
			super(alarmAgent);
		}

		@Override
		public void action() {
			ACLMessage msg = myAgent.receive();
			if (msg != null) {
				if (msg.getConversationId().equalsIgnoreCase(LOCATION_REQUEST)) {
					ACLMessage tellLocation = new ACLMessage(ACLMessage.INFORM);
					tellLocation.setConversationId(LOCATION_INFO);
					tellLocation.addReceiver(msg.getSender());
					getCurrentLocation();
					tellLocation.setContent(myAgent.getAID() + ":" + currentLocation.getLatitude() + ":" + currentLocation.getLongitude());
					send(tellLocation);
				} else if (msg.getConversationId().equalsIgnoreCase(LOCATION_INFO)) {
					locationMsgReceivedFrom.put(msg.getSender().getLocalName(), msg);
					//Checking if we got response from all friends who were pinged
					//Assuming that everyone does.
					if (locationMsgReceivedFrom.keySet().size() == friendsList.size()) {
						String nearest = findNearest();
						ACLMessage tellFriend2WakeMeUp = new ACLMessage(ACLMessage.INFORM);
						tellFriend2WakeMeUp.setConversationId(WAKEOTHERGUYUPMESSAGE);
						tellFriend2WakeMeUp.addReceiver(new AID(nearest, AID.ISGUID));
						tellFriend2WakeMeUp.setContent(myAgent.getLocalName());
						send(tellFriend2WakeMeUp);
					}
				} else if (msg.getConversationId().equalsIgnoreCase(WAKEOTHERGUYUPMESSAGE)) {
					notify2WakeDFriend2App(msg.getContent());
				}
			}
		}
	}

	//Since this behavior is invoked only after friendsList is updated, no need to be concerned about null check
	class AskFriends extends OneShotBehaviour {

		private static final long serialVersionUID = 1L;
		private String alarmUserName;

		public AskFriends(String name) {
			this.alarmUserName = name;
		}

		@Override
		public void action() {
			ACLMessage notifyMsg = new ACLMessage(ACLMessage.INFORM);
			notifyMsg.setConversationId(LOCATION_REQUEST);
			getAllAgents();
			for (AMSAgentDescription agent : agents) {
				AID thisAID = (AID) agent.getName();
				if (friendsList.contains(thisAID.getLocalName()) && !thisAID.getLocalName().equals(myAgent.getLocalName()))
					notifyMsg.addReceiver(thisAID);
			}
			notifyMsg.setContent(alarmUserName);
			System.out.println("Asked all friends for their location specifying my name= "+alarmUserName);
			send(notifyMsg);
		}
	}

	@Override
	public void requestFriends(String s) {
		addBehaviour(new AskFriends(s));
	}

	@Override
	public void populateFriendsList(ArrayList<String> friends) {
		this.friendsList = friends;
	}

	@Override
	public void populateLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}
	
	@SuppressLint("UseValueOf")
	public void getAllAgents() {
		try {
			SearchConstraints c = new SearchConstraints();
			c.setMaxResults(new Long(-1));
			agents = AMSService.search(this, new AMSAgentDescription(), c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String findNearest() {
		double minDistance = 9999999.99;
		String nearestFriend = null;
		for (Entry<String, ACLMessage> entry : locationMsgReceivedFrom.entrySet()) {
			String[] messageContent = entry.getValue().getContent().split(":");
			String latitude = messageContent[1];
			String longitude = messageContent[2];
			double retDistance = getDistance(currentLocation.getLatitude(), currentLocation.getLongitude(), Double.valueOf(latitude), Double.valueOf(longitude));
			if (retDistance < minDistance) {
				minDistance = retDistance;
				nearestFriend = entry.getKey();
			}
		}
		return nearestFriend;
	}

	private static double getDistance(double lat1, double lon1, double lat2, double lon2) {
		final double Radius = 6371 * 1E3; // Earth's mean radius
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return Radius * c;
	}



	/*
	 * private void handleUnexpected(ACLMessage msg) { if
	 * (logger.isLoggable(Level.WARNING)) { logger.log(Level.WARNING,
	 * "Unexpected message received from " + msg.getSender().getName());
	 * logger.log(Level.WARNING, "Content is: " + msg.getContent()); } }
	 */

}
