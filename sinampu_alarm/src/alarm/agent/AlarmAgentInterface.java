package alarm.agent;

import java.util.ArrayList;

import android.location.Location;

public interface AlarmAgentInterface {

	public void requestFriends(String s);

	public void populateFriendsList(ArrayList<String> friends);

	public void populateLocation(Location location);
}
