package sinampu_p4;


import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class BusAgent extends Agent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void setup() {

		Object[] args = getArguments();
		String userName = "";
		String amountOfDelay = "";
		if (args == null || args.length <= 1) {
			System.out.println("Invalid Number of arguments. Please specify intended user and amount of delay");
			return;
		}
		if (args != null && args.length > 0) {
			userName = args[0].toString();
			amountOfDelay = args[1].toString();
			try{
				Integer.valueOf(amountOfDelay);
			} catch (Exception e){
				System.err.println(e.getMessage() + " -*Please enter Integer value which indicates amount of delay in minutes");
			}
		}

		DFAgentDescription dfAgentDescription = new DFAgentDescription();
		dfAgentDescription.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("bus");
		sd.setName("bus-agent");
		dfAgentDescription.addServices(sd);
		try {
			DFService.register(this, dfAgentDescription);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		try {
			addBehaviour(new DelaySimulateBehaviour(userName, amountOfDelay));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("HELLO: bus-agent " + getAID().getName() + " is ready.");
	}

	@Override
	protected void takeDown() {
		try {
			DFService.deregister(this); // Unregistering buyer-agent.
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		System.out.println("TERMINATING: bus-agent " + getAID().getName());

	}

	class DelaySimulateBehaviour extends Behaviour {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String username;
		private String amountOfDelay;

		public DelaySimulateBehaviour(String username, String amountOfDelay) {
			this.username = username;
			this.amountOfDelay = amountOfDelay;
		}

		@Override
		public void action() {
			ACLMessage notifyMsg = new ACLMessage(ACLMessage.INFORM);
			String message = username + ":" + amountOfDelay;
			notifyMsg.setConversationId("BUS_DELAYED");
			notifyMsg.setContent(message);
			notifyMsg.addReceiver(new AID(username,AID.ISLOCALNAME));
			System.out.println("Sending message = " + message +" to " + new AID(username,AID.ISLOCALNAME));
			send(notifyMsg);
		}

		@Override
		public boolean done() {
			return true;
		}

	}

}
